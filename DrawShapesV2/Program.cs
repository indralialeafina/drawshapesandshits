﻿using ShapesV2;
using SixLabors.ImageSharp;
using Rectangle = ShapesV2.Rectangle;

namespace DrawShapesV2
{
    class Program
    {
        static void Main(string[] args)
        {
            string docs = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            string image_file = "Drawing.png";
            string filename = Path.Combine(docs, image_file);
            Canvas canvas = new Canvas(800,600, Color.Black);
            
            // Dessiner un Polygone
            List < ShapesV2.Point > pointList = new List<ShapesV2.Point>
            {
                new ShapesV2.Point(200, 50), new ShapesV2.Point(x:300, 175),
                new ShapesV2.Point(375, 200), new ShapesV2.Point(300, 275),
                new ShapesV2.Point(100, 120)
            };
            Polygone poly = new Polygone(pointList, Color.Red);
            poly.Draw(canvas,true);
            
            // Dessiner des smiley faces
            SmileyFace smileMf = new SmileyFace(new ShapesV2.Point(60, 60), 50,Mood.Happy, Color.Yellow, Color.Black,true);
            smileMf.Draw(canvas);
            SmileyFace smileAgainMf = new SmileyFace(new ShapesV2.Point(700, 90), 75, Mood.Sad, Color.Blue, Color.Cyan,true);
            smileAgainMf.Draw(canvas);
            SmileyFace smileOnceMoreMf = new SmileyFace(new ShapesV2.Point(90, 500), 80, Mood.Neutral, Color.Red, Color.Black);
            smileOnceMoreMf.Draw(canvas);
            
            // dessiner triangle
            Triangle triForce = new Triangle(
                new ShapesV2.Point(50, 400), new ShapesV2.Point(200, 575),
                new ShapesV2.Point(375, 350), Color.Purple);
            triForce.Draw(canvas);
            
            // dessiner cercles vides et pleins
            Circle cercleVide = new Circle(new ShapesV2.Point(475, 75), 60, Color.Orange);
            cercleVide.Draw(canvas);
            Circle cerclePlein = new Circle(new ShapesV2.Point(400, 550), 45, Color.Aquamarine);
            cerclePlein.DrawFull(canvas);
            
            // dessiner lignes verticales, horizontales et diagonale quelconque
            HLine ligneHoriz = new HLine(new ShapesV2.Point(200, 50), 150, Color.Blue);
            ligneHoriz.Draw(canvas);
            VLine ligneVert = new VLine(new ShapesV2.Point(100, 120), 250, Color.Brown);
            ligneVert.Draw(canvas);
            Line ligne = new Line(new ShapesV2.Point(300, 275), new ShapesV2.Point(100, 120), Color.Brown);
            ligne.Draw(canvas);
            
            // dessiner rectangles et carrés vides et pleins
            Rectangle rectPlein = new Rectangle(new ShapesV2.Point(550, 350), 75, 180, Color.Green);
            rectPlein.DrawFull(canvas);
            Rectangle rectVide = new Rectangle(new ShapesV2.Point(500, 250), 180, 20, Color.Magenta);
            rectVide.Draw(canvas);
            Square carreVide = new Square(new ShapesV2.Point(375, 200), 100, Color.Blue);
            carreVide.Draw(canvas);
            
            Console.WriteLine($"Writing image in file: {filename}");
            canvas.Save(filename);
        }
    }
}