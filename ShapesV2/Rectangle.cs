﻿using ShapesV1;
using SixLabors.ImageSharp;

namespace ShapesV2;

public class Rectangle
{
    public Point StartingPoint { get; set; }
    public int Width { get; set; }
    public int Height { get; set; }
    public Color Drawcolor { get; set; }

    public Rectangle(Point startingPoint, int width, int height, Color drawcolor)
    {
        StartingPoint = startingPoint;
        Width = width;
        Height = height;
        Drawcolor = drawcolor;
    }

    public void Draw(Canvas canvas)
    {
        VLine leftSide = new VLine(this.StartingPoint, Height, Drawcolor);
        HLine top = new HLine(this.StartingPoint, Width, Drawcolor);
        HLine bottom = new HLine(new Point(StartingPoint.X, StartingPoint.Y + Height -1), Width, Drawcolor);
        VLine rightSide = new VLine(new Point(StartingPoint.X + Width -1, StartingPoint.Y), Height, Drawcolor);
        leftSide.Draw(canvas);
        top.Draw(canvas);
        bottom.Draw(canvas);
        rightSide.Draw(canvas);
    }
    public void DrawFull(Canvas canvas)
    {
        for (int i = 0+StartingPoint.Y;i<StartingPoint.Y +Height;i++)
        {
            HLine lineToDraw = new HLine(new Point(StartingPoint.X, i), Width, Drawcolor);
            lineToDraw.Draw(canvas);
        }
    }
}