﻿using SixLabors.ImageSharp;

namespace ShapesV2;
public enum Mood {Happy,Neutral,Sad}

public class SmileyFace
{
    public Point CenterPoint { get; set; }
    public int Radius { get; set; }
    public Mood SmileyMood { get; set; }
    public Color BackGroundColor { get; set; }
    public Color DetailColor { get; set; }
    public bool HasGlasses { get; set; }
    public SmileyFace(Point centerPoint, int radius, Mood smileyMood, Color backGroundColor, Color detailColor, bool hasGlasses=false)
    {
        CenterPoint = centerPoint;
        SmileyMood = smileyMood;
        BackGroundColor = backGroundColor;
        DetailColor = detailColor;
        Radius = radius;
        HasGlasses = hasGlasses;
    }
    public void Draw(Canvas canvas)
    {
        Circle face = new Circle(CenterPoint, Radius, BackGroundColor);
        face.DrawFull(canvas);
        DrawEyes(face,canvas);
        DrawSmile(face, canvas);
    }
    private void DrawSmile(Circle face, Canvas canvas)
    {
        // initialiser variables de taille et de localisation du sourire
        double smileWidthRatio = face.Radius / 2.0d;
        double smileHeightRatio = face.Radius / 8.0d;
        double distanceFromCenter = 1.8d;
        double locationX = CenterPoint.X + (face.Radius/distanceFromCenter * Math.Cos(90/57.2958d));
        double locationY = CenterPoint.Y + (face.Radius/distanceFromCenter * Math.Sin(90/57.2958d));
        Point smileCenterPoint = new Point((int)locationX,(int) locationY);
        
        switch (SmileyMood)
        {
            case Mood.Happy:
                Point happySmileCenter = new Point(smileCenterPoint.X, smileCenterPoint.Y - Radius);
                double movableSmileRadius = face.Radius;
                for (double radiusOffSet = -1; radiusOffSet < 1; radiusOffSet+=0.1d)
                {
                    movableSmileRadius += radiusOffSet;
                    for (double angle = 55.0; angle < 125; angle+=0.1d)
                    {
                        double locX = happySmileCenter.X + (movableSmileRadius * Math.Cos(angle/57.2958d));
                        double locY = happySmileCenter.Y + (movableSmileRadius * Math.Sin(angle/57.2958d));
                        canvas.SetPixel((int)locX,(int)locY,DetailColor);
                    }
                }
                break;
            
            case Mood.Neutral:
                Point smileTopCorner = new Point(smileCenterPoint.X - (int) smileWidthRatio / 2,
                    smileCenterPoint.Y - (int) smileHeightRatio / 2);
                Rectangle smile = new Rectangle(smileTopCorner, (int) smileWidthRatio, (int) smileHeightRatio,
                    DetailColor);
                smile.DrawFull(canvas);
                break;
            
            case Mood.Sad:
                Point unHappySmileCenter = new Point(smileCenterPoint.X, smileCenterPoint.Y + Radius);
                double movableRadius = face.Radius;
                for (double radiusOffSet = -1; radiusOffSet < 1; radiusOffSet+=0.1d)
                {
                    movableRadius += radiusOffSet;
                    for (double angle = -120.0; angle < -60; angle += 0.1d)
                    {
                        double locX = unHappySmileCenter.X + (movableRadius * Math.Cos(angle / 57.2958d));
                        double locY = unHappySmileCenter.Y + (movableRadius * Math.Sin(angle / 57.2958d));
                        canvas.SetPixel((int) locX, (int) locY, DetailColor);
                    }
                }
                break;
        }
    }
    private void DrawEyes(Circle face, Canvas canvas)
    {
        // initialiser variables pour yeux et lunettes (optionelles)
        int eyeRadius = face.Radius / 6;
        int glassesRadius = face.Radius / 4;
        double angleForRightEye = -45.00d;
        double angleForLeftEye = angleForRightEye - 90.0d;
        double distanceFromCenter = 1.90d;
        
        // localiser et dessiner oeil droit
        double locationX = CenterPoint.X + (face.Radius/distanceFromCenter * Math.Cos(angleForRightEye/57.2958d));
        double locationY = CenterPoint.Y + (face.Radius/distanceFromCenter * Math.Sin(angleForRightEye/57.2958d));
        Point rightEyeCenter = new Point((int) locationX, (int) locationY);
        Circle rightEye = new Circle(rightEyeCenter, eyeRadius, DetailColor);
        rightEye.DrawFull(canvas);
        // localiser et dessiner oeil gauche
        locationX = CenterPoint.X + (face.Radius/distanceFromCenter * Math.Cos(angleForLeftEye/57.2958d));
        locationY = CenterPoint.Y + (face.Radius/distanceFromCenter * Math.Sin(angleForLeftEye/57.2958d));
        Point leftEyeCenter = new Point((int) locationX, (int) locationY);
        Circle leftEye = new Circle(leftEyeCenter, eyeRadius, DetailColor);
        leftEye.DrawFull(canvas);
        
        if (HasGlasses)
        {
            // dessiner lentilles sur oeil gauche et droit
            Circle rightMonocle = new Circle(rightEyeCenter, glassesRadius, DetailColor);
            rightMonocle.Draw(canvas);
            Circle leftMonocle = new Circle(leftEyeCenter, glassesRadius, DetailColor);
            leftMonocle.Draw(canvas);
            
            // dessiner une ligne entre les deux monocles des lunettes
            Line lunetteLink = new Line(new Point(leftEyeCenter.X + glassesRadius, leftEyeCenter.Y),
                new Point(rightEyeCenter.X - glassesRadius, rightEyeCenter.Y), DetailColor);
            lunetteLink.Draw(canvas);
            
            // dessiner branche droite
            double locationRightSideX = CenterPoint.X + (face.Radius * Math.Cos(-30/57.2958d)); // ou 180/math.PI
            double locationRightSideY = CenterPoint.Y + (face.Radius * Math.Sin(-30/57.2958d));
            Point rightSideOfRight = new Point(rightEyeCenter.X + glassesRadius, rightEyeCenter.Y);
            Point rightSideOfFace = new Point((int) locationRightSideX, (int) locationRightSideY);
            Line rightBranch = new Line(rightSideOfRight, rightSideOfFace, DetailColor);
            rightBranch.Draw(canvas);
            
            // dessiner branche gauche
            double locationLeftSideX = CenterPoint.X + (face.Radius * Math.Cos(210/57.2958d));
            double locationLeftSideY = CenterPoint.Y + (face.Radius * Math.Sin(210/57.2958d));
            Point leftSideOfLeft = new Point(leftEyeCenter.X - glassesRadius, leftEyeCenter.Y);
            Point leftSideOfFace = new Point((int) locationLeftSideX, (int) locationLeftSideY);
            Line leftBranch = new Line(leftSideOfLeft, leftSideOfFace, DetailColor);
            leftBranch.Draw(canvas);
        }
    }
}