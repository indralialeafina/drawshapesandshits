﻿using SixLabors.ImageSharp;

namespace ShapesV2;

public class Polygone
{
    public Color DrawColor { get; set; }
    
    public List<Point> PointsList { get; set; }

 
    public Polygone(List<Point> pointsList, Color drawColor)
    {
        PointsList = pointsList;
        DrawColor = drawColor;
    }
    public void Draw(Canvas canvas, bool markersOn = false)
    {
        // verifier le nombre de points pour trouver le dernier
        Point firstPoint = PointsList[0];
        Point LastPoint = PointsList[PointsList.Count - 1];
        Console.WriteLine("Premier point " + firstPoint.ToString() + " dernier point " + LastPoint.ToString());
        // on loop pour dessiner les lignes une a une jusqu'au dernier element
        for (int indexLignes = 0; indexLignes < PointsList.Count-1; indexLignes++)
        {
            // faire ligne du point index au prochain
            Line TempLine = new Line(PointsList[indexLignes], PointsList[indexLignes + 1], DrawColor);
            TempLine.Draw(canvas);
            // dessiner point sur jonction
            if (markersOn)
            {
                Circle marker = new Circle(PointsList[indexLignes + 1], 3,DrawColor);
                marker.DrawFull(canvas);
            }
        }
        // si premier et dernier points ne sont pas les memes coordonnee, finir la forme en les reliant
        if (firstPoint.X != LastPoint.X || firstPoint.Y != LastPoint.Y)
        {
            Line TempLine = new Line(LastPoint, firstPoint, DrawColor);
            TempLine.Draw(canvas);
            if (markersOn)
            {
                Circle marker = new Circle(firstPoint, 3,DrawColor);
                marker.DrawFull(canvas);
            }
        }
    }
}