﻿using SixLabors.ImageSharp;

namespace ShapesV2;

public class Square
{
    public Point StartingPoint { get; set; }
    public int Size { get; set; }
    public Color DrawColor { get; set; }

    public Square(Point startingPoint, int size, Color drawColor)
    {
        StartingPoint = startingPoint;
        Size = size;
        DrawColor = drawColor;
    }

    public void Draw(Canvas canvas)
    {
        Rectangle squareRectangle = new Rectangle(StartingPoint, Size, Size, DrawColor);
        squareRectangle.Draw(canvas);
    }
    public void DrawFull(Canvas canvas)
    {
        Rectangle squareRectangle = new Rectangle(StartingPoint, Size, Size, DrawColor);
        squareRectangle.DrawFull(canvas);
    }
}