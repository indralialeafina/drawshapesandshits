﻿using ShapesV1;
using SixLabors.ImageSharp;
namespace ShapesV2;

public class Circle
{
    public Color DrawColor { get; set; }
    public Point CenterPoint { get; set; }
    public int Radius { get; set; }

    public Circle(Point centerPoint, int radius, Color drawColor)
    {
        DrawColor = drawColor;
        CenterPoint = centerPoint;
        Radius = radius;
    }

    public void Draw(Canvas canvas)
    {
        double factor = 1;
        factor = factor / (Radius * 0.1);
        for (double angle = 0; angle < 360; angle+=factor)
        {
            double locationX = CenterPoint.X + (Radius * Math.Cos(angle));
            double locationY = CenterPoint.Y + (Radius * Math.Sin(angle));
            
            canvas.SetPixel((int)locationX, (int)locationY, DrawColor);
        }
    }

    public void DrawFull(Canvas canvas)
    {
        int cos45 = Convert.ToInt32(Math.Round(Radius * Math.Cos(Math.PI/4), MidpointRounding.AwayFromZero));
        
        for (int i = 0; i <= cos45; i++)
        {
            int j = Convert.ToInt32(Math.Round(Math.Sqrt(Radius * Radius - i * i), MidpointRounding.AwayFromZero));
            
            HLine lineToDraw = new HLine(new Point(CenterPoint.X - i, CenterPoint.Y + j), i *2, DrawColor);
            lineToDraw.Draw(canvas);
            HLine lineToDraw2 = new HLine(new Point(CenterPoint.X - i, CenterPoint.Y - j), i * 2, DrawColor);
            lineToDraw2.Draw(canvas);
            HLine lineToDraw3 = new HLine(new Point(CenterPoint.X - j, CenterPoint.Y + i), j * 2, DrawColor);
            lineToDraw3.Draw(canvas);
            HLine lineToDraw4 = new HLine(new Point(CenterPoint.X - j, CenterPoint.Y - i), j * 2, DrawColor);
            lineToDraw4.Draw(canvas);
        }
    }
}