﻿using System;
using System.Collections.Generic;
using ExamenLib1;

namespace Examen1App
{
    internal class Program
    {
        public static List<PlayList> ListeChoix = new List<PlayList>();
        public static bool IsProgramRunning = true;
        
        public static void Main(string[] args)
        {
            while (IsProgramRunning)
            {
                Console.Clear();
                if (ListeChoix.Count == 0)
                {
                    GeneratePlaylists();
                }
                int userChoice = 0;

                for (int i = 0; i < ListeChoix.Count; i++)
                {
                    Console.WriteLine((i + 1) + " : " + ListeChoix[i].Name);
                }

                do
                {
                    Console.Write("Entrez votre choix de playlist : ");
                    int.TryParse(Console.ReadLine(), out userChoice);
                } while (userChoice < 0 || userChoice > ListeChoix.Count);

                Console.WriteLine("Vous avez choisi : " + ListeChoix[userChoice - 1].Name + "\n");

                Console.WriteLine(ListeChoix[userChoice - 1]);
                Console.WriteLine("La liste à une durée de " + FormatTime(ListeChoix[userChoice-1].TotalLength()));

                string resetOn = "";
                do
                {
                    Console.Write("Choisir une autre liste O/N ? ");
                    resetOn = Console.ReadLine().ToUpper();
                } while (resetOn != "N" && resetOn != "O");

                if (resetOn == "N")
                {
                    IsProgramRunning = false;
                    Console.WriteLine("Merci et bonne journée");
                }
            }
        }

        private static string FormatTime(int seconds)
        {
            int runTimeMinutes = seconds / 60;
            int runTimeSeconds = seconds % 60;
            string padding0 = runTimeSeconds < 10 ? "0" : "";
            return $"{runTimeMinutes}:{padding0}{runTimeSeconds}";
        }

        private static void GeneratePlaylists()
        {
            PlayList tinaTournee = new PlayList("Tina Tournée");
            tinaTournee.SongList.Add(new Song("What's love", "Tina Tournée", 121));
            tinaTournee.SongList.Add(new Song("Z'got to do", "Tina Tournée", 144));
            tinaTournee.SongList.Add(new Song("et With it", "Tina Tournée", 155));
            tinaTournee.SongList.Add(new Song("Peel off the paper before to fix it on the floor", "Tina Tournée", 210 ));
            ListeChoix.Add(tinaTournee);

            PlayList tataBoutlamine = new PlayList("Tata Boutlamine");
            tataBoutlamine.SongList.Add(new Song("Do dièse", "Tata Boutlamine",212));
            tataBoutlamine.SongList.Add(new Song("Narine", "Tata Boutlamine",280));
            tataBoutlamine.SongList.Add(new Song("Sol bémol", "Tata Boutlamine",212));
            tataBoutlamine.SongList.Add(new Song("Mi majeur", "Tata Boutlamine",92));
            ListeChoix.Add(tataBoutlamine);

            PlayList celestinLaperriere = new PlayList("Celestin Laperrière");
            celestinLaperriere.SongList.Add(new Song("Je pleure sur tes valises", "Celestin Laperrière", 260));
            celestinLaperriere.SongList.Add(new Song("Toi et moi dans l'couloir", "Celestin Laperrière", 182));
            celestinLaperriere.SongList.Add(new Song("Pas vraiment interessé", "Celestin Laperrière", 69));
            celestinLaperriere.SongList.Add(new Song("Derrière cette grange", "Celestin Laperrière", 320));
            ListeChoix.Add(celestinLaperriere);
            
        }
    }
}