﻿using System.Collections.Generic;

namespace ExamenLib1
{
    public class PlayList
    {
        public string Name { get; set; }
        public List<Song> SongList { get; set; }
        public int Count { get => SongList.Count;}

        public PlayList(string name)
        {
            Name = name;
            SongList = new List<Song>();
        }

        public override string ToString()
        {
            string compositeString = $"The playlist '" + Name + "' has " + Count + " songs\n";
            foreach (Song song in SongList)
            {
                compositeString += song + "\n";
            }
            return compositeString;
        }

        public int TotalLength()
        {
            int totalSeconds = 0;
            foreach (Song song in SongList)
            {
                totalSeconds += song.Seconds;
            }
            return totalSeconds;
        }
    }
}