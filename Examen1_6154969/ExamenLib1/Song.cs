﻿using System;

namespace ExamenLib1
{
    public class Song
    {
        public static int NextId { get; private set; } = 1;
        public int Id { get; set; }
        public string Title { get; set; }
        public string Artist { get; set; }
        public int Seconds { get; set; }
        public double Minutes { get; set;}

        public Song(string title, string artist, int seconds)
        {
            Id = NextId;
            NextId++;
            Title = title;
            Artist = artist;
            Seconds = seconds;
            Minutes = (double) seconds / 60;
        }

        public override string ToString()
        {
            int runTimeMinutes = Seconds / 60;
            int runTimeSeconds = Seconds % 60;
            string padding0 = runTimeSeconds < 10 ? "0" : "";
            return $"{Id}: {Title} ({Artist}) {runTimeMinutes}:{padding0}{runTimeSeconds}";
        }

        public override bool Equals(object obj)
        {
            if (obj == null || this.GetType() != obj.GetType())
            {
                return false;
            }
            Song other = (Song)obj;
            return Id == other.Id;
        }
        
        public override int GetHashCode()
        {
            return Id;
        }
    }
}